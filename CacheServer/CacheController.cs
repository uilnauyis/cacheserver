﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CacheServer
{
    public class CacheController : ApiController
    {
        private static HttpClient _client = new HttpClient();
        public static HashSet<string> _cachedFileNames = new HashSet<string>();

        [HttpGet]
        public async Task<IHttpActionResult> ListFiles()
        {
            try
            {
                var response = await _client.GetAsync("http://localhost:9000/server/ListFiles");
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return Ok("Server response error");
                }
                var responseContentStr = await response.Content.ReadAsStringAsync();
                var fileNames = JsonConvert.DeserializeObject<List<string>>(responseContentStr);
                return Ok(fileNames);
            }
            catch (Exception err) {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Exception happens while attempting to process the file"));
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> DownloadFile([FromUri]string fileName = null)
        {
            byte[] fileContent;
            if (_cachedFileNames.Contains(fileName))
            {
                fileContent = await LoadFile(fileName);
                await UpdateLog(fileName, true);
            }
            else
            {
                try
                {
                    var response = await _client.GetAsync($"http://localhost:9000/server/DownloadFile?fileName={fileName}");
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return Ok("Server response error");
                    }
                    fileContent = await response.Content.ReadAsByteArrayAsync();
                    await SaveFileInCache(fileName, fileContent);
                    _cachedFileNames.Add(fileName);
                }
                catch (Exception err)
                {
                    return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                        "Exception happens while attempting to process the file"));
                }
                await UpdateLog(fileName, false);
            }
            HttpResponseMessage newResponse = new HttpResponseMessage();
            newResponse.StatusCode = HttpStatusCode.OK;
            newResponse.Content = new StreamContent(new MemoryStream(fileContent));
            return ResponseMessage(newResponse);
        }

        private async Task<Byte[]> LoadFile(string fileName)
        {
            MemoryStream responseStream = new MemoryStream();
            byte[] bytes = new byte[0];
            await Task.Run(() =>
            {
                bytes = File.ReadAllBytes($"{Util.CACHE_STORAGE_PATH}/{fileName}");
            });
            return bytes;
        }

        private async Task SaveFileInCache(string fileName, byte[] content)
        {
            using (FileStream stream = File.Open($"{Util.CACHE_STORAGE_PATH}/{fileName}", FileMode.CreateNew))
            {
                await stream.WriteAsync(content, 0, content.Length);
            }
        }

        private async Task UpdateLog(string fileName, bool fromCache)
        {
            using (var writer = new StreamWriter(Util.LOG_PATH, true))
            {
                await writer.WriteLineAsync($"user request: file {fileName} at {DateTime.Now}");
                if (fromCache)
                {
                    await writer.WriteLineAsync($"response: cached file {fileName}");
                }
                else
                {
                    await writer.WriteLineAsync($"response: file {fileName} downloaded from the server");
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CacheServer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private FileSystemWatcher filesWatcher;
		private FileSystemWatcher logWatcher;

		private Thread serverThread;

		public MainWindow()
		{
			InitializeComponent();
			Util.CreateStorage();
			Util.CreateLog();
			serverThread = new Thread(Server.Start);
			startFilesWatcher();
			startLogWatcher();
			serverThread.Start();
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			serverThread.Abort();
			base.OnClosing(e);
		}

		private void startFilesWatcher()
		{
			filesWatcher = new FileSystemWatcher();
			filesWatcher.Path = Util.CACHE_STORAGE_PATH;
			filesWatcher.NotifyFilter = NotifyFilters.LastWrite;
			filesWatcher.Changed += new FileSystemEventHandler(OnFilesChanged);
			filesWatcher.Created += new FileSystemEventHandler(OnFilesChanged);
			filesWatcher.Deleted += new FileSystemEventHandler(OnFilesChanged);
			filesWatcher.EnableRaisingEvents = true;
		}

		private void startLogWatcher()
		{
			logWatcher = new FileSystemWatcher();
			logWatcher.Path = System.IO.Path.GetDirectoryName(Util.LOG_PATH);
			logWatcher.Filter = System.IO.Path.GetFileName(Util.LOG_PATH);
			logWatcher.NotifyFilter = NotifyFilters.LastWrite;
			logWatcher.Changed += new FileSystemEventHandler(OnLogChanged);
			logWatcher.EnableRaisingEvents = true;
		}

		private void OnFilesChanged(object source, FileSystemEventArgs e)
		{
			this.Dispatcher.Invoke(() =>
			{
				avilableFiles.Text = "";
				foreach (string file in Directory.EnumerateFiles(Util.CACHE_STORAGE_PATH))
				{
					avilableFiles.Text += System.IO.Path.GetFileName(file) + '\n';
				};
			});
		}

		private void OnLogChanged(object source, FileSystemEventArgs e)
		{
			this.Dispatcher.Invoke(() =>
			{
				MemoryStream responseStream = new MemoryStream();
				logs.Text = File.ReadAllText(Util.LOG_PATH);
			});
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			Util.ClearCache();
			await UpdateLogForClearingCache();
			OnFilesChanged(null, null);
			OnLogChanged(null, null);
			CacheController._cachedFileNames.Clear();
		}

		private async Task UpdateLogForClearingCache()
		{
			using (var writer = new StreamWriter(Util.LOG_PATH, true))
			{
				await writer.WriteLineAsync("Clear cache");
			}
		}
	}
}

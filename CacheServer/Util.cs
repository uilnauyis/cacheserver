﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheServer
{
    class Util
    {
        public const string CACHE_STORAGE_PATH = "cachedFiles";
        public const string LOG_DIRECTORY_PATH = "log";
        public const string LOG_PATH = "log/log.txt";

        public static void CreateStorage()
        {
            Directory.CreateDirectory(CACHE_STORAGE_PATH);
            DirectoryInfo di = new DirectoryInfo(CACHE_STORAGE_PATH);
            foreach (var file in di.EnumerateFiles())
            {
                file.Delete();
            }
        }

        public static void CreateLog()
        {
            if (File.Exists(LOG_PATH))
            {
                File.Delete(LOG_PATH);
            }
            Directory.CreateDirectory(LOG_DIRECTORY_PATH);
            File.CreateText(LOG_PATH);
        }

        public static void ClearCache()
        {
            CreateStorage();
        }
    }
}
